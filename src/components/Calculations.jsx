import { Fragment, useState, useMemo } from "react";

const Calculations = () => {
  const [a, setA] = useState(1);
  const [b, setB] = useState(2);
  const [c, setC] = useState(0);

  const calcValue = useMemo(() => calc(), [a, b]);
  // const calcValue = calc();

  function calc() {
    console.log('starting calc!');
    let calcValue = 0;
    for (let i = 0; i < 1000; i++) {
      calcValue = calcValue + a + b;
    }
    return calcValue;
  }

  function changeA(e) {
    setA(Number(e.target.value));
  }

  function changeB(e) {
    setB(Number(e.target.value));
  }

  function changeC(e) {
    setC(Number(e.target.value));
  }

  return (
    <Fragment>
      <h1>Calculations</h1>
      <input type="number" onChange={changeA} value={a} />
      <input type="number" onChange={changeB} value={b} />
      <input type="number" onChange={changeC} value={c} />
      {`Calc value: ${calcValue}`}
    </Fragment>
  );
};

export default Calculations;