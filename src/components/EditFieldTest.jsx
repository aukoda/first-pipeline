import React, { useState } from 'react';
import styled from 'styled-components';

const EditFieldTest = () => {
  const [isEdit, setIsEdit] = useState(false);
  const [firstName, setFirstName] = useState('Josiah');
  const [input, setInput] = useState('');

  function onEditName(e) {
    e.preventDefault();
    console.log('Name changed to: ', input);
  }

  const toggleEdit = () => setIsEdit(canEdit => !canEdit);

  const updateInput = e => setInput(e.target.value);

  return (
    <Form onSubmit={onEditName}>
      {isEdit ? (
        <FormField>
          <div className="input-group">
            <Input placeholder='name' value={firstName} onChange={updateInput} />
          </div>
          <Button onClick={toggleEdit}>Save</Button>
        </FormField>
      ) : (
        <div>
          <span>{firstName}</span>
          <Button onClick={toggleEdit}>Edit</Button>
        </div>
      )}
    </Form>
  );
};

export default EditFieldTest;

const Form = styled.form`
  // outline: 3px solid crimson;
  padding: 0 2rem;

  .input-group {
    display: inline-flex;
    gap: .5rem;
    flex-direction: column;
  }
`;

const FormField = styled.div`
  display: inline-flex;
  gap: 1rem;
`;

const Input = styled.input`
  padding: .8rem 1rem;
  background: gainsboro;
  border: none;
  color: #333;
  font-size: 1.2rem;
`;

const Button = styled.button`
  padding: .8rem 1.5rem;
`;