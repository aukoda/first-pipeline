const Search = ({ onInputChange }) => {
  console.log("Search component rendered");
  return (
    <input type="text" onChange={onInputChange} />
  );
};

export default Search;