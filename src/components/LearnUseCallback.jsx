import { Fragment, memo } from "react";

function BigList({ list, onItemClick }) {
  console.log('list rendered');
  console.log(list);
  const map = item => <div key={item.name} onClick={onItemClick}><span>{item.name}</span> - <span>{item.age}</span></div>;
  return <Fragment>{list.map(map)}</Fragment>;
};

function LearnUseCallback() {
  console.log('callback comp rendered');
  function factory() {
    return (a, b) => a + b;
  }
  const list = [
    { name: "Kate", age: 10 },
    { name: "Gerald", age: 89 },
    { name: "Anthony", age: 50 },
    { name: "Steve", age: 34 },
    { name: "Lars", age: 13 },
    { name: "Henriette", age: 20 },
    { name: "Derick", age: 60 },
    { name: "Bolanle", age: 43 },
    { name: "Chris", age: 67 },
    { name: "John", age: 56 },
  ];

  function onItemClick() {
    console.log('Item clicked');
  }
  // const sum1 = factory();
  // const sum2 = factory();
  // if (sum1 === sum2) {
  //   console.log("Functions are equal");
  // } else {
  //   console.log("Functions are not equal");
  // }

  return (
    <Fragment>
      <h1>Learn Use Callback</h1>
      <BigList list={list} onItemClick={onItemClick} />
    </Fragment>
  );
}

export default LearnUseCallback;