import React, { useState, useRef } from 'react';
import Calculations from './components/Calculations';
import EditFieldTest from './components/EditFieldTest';
import LearnUseCallback from './components/LearnUseCallback';
// import throttle from "lodash/throttle";
import Search from "./components/Search";

function App() {
  // const [text, setText] = useState("");
  const textRef = useRef();
  console.log('App Render');

  function onInputChange(e) {
    textRef.current.innerText = `Input Text: ${e.target.value}`;
  }

  // const throttledClick = throttle(handleClick, 5000);
  // function handleClick() {
  //   console.log("Clicked");
  // }
  // <h1 ref={textRef} /> 

  return (
    <React.Fragment>
      <h1>Testing Edit Fields </h1>
      {/* <Search onInputChange={onInputChange} /> */}
      {/* 
        <Calculations /> 
        <LearnUseCallback />
      */}
      <EditFieldTest />
    </React.Fragment>
  );
}

export default App;
